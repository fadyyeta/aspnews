﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WCM_settings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblUN.Text="Welcome "+ Session["un"].ToString();
    }
    protected void btnAddUser_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/WCM/reg.aspx");
    }
    protected void btnAddArticle_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/WCM/articles.aspx");
    }
    protected void btnEditArticle_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/WCM/editArticle.aspx");
    }
}