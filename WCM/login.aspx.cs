﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;

public partial class WCM_login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection("server=.;Integrated Security=SSPI; database=News");
        SqlCommand sql = new SqlCommand("Select * from users where userName=@userName and password=@password",con);
        sql.Parameters.AddWithValue("@userName", txtUserLogin.Text);
        sql.Parameters.AddWithValue("@password", txtPassLogin.Text);

        SqlDataAdapter Adapter = new SqlDataAdapter(sql);
        DataSet ds = new DataSet();
        Adapter.Fill(ds,"users");
        if (ds.Tables["users"].Rows.Count==0)
        {
            Response.Write("Invalid User");
        }
        else
        {
            Session["un"] = txtUserLogin.Text;
            Response.Redirect("~/WCM/reg.aspx");
        }
    }
}