﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;


public partial class WCM_articles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAddArticle_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection("server=.;Integrated Security=SSPI; database=News");
        con.Open();


        SqlCommand sql = new SqlCommand("insert into articles(title,details,photo) values (@title,@details,@photo)", con);
        sql.Parameters.AddWithValue("@title", txtTitle.Text);
        sql.Parameters.AddWithValue("@details", txtDetails.Text);
        string strImg = System.IO.Path.GetFileName(File1.PostedFile.FileName);

        sql.Parameters.Add("@photo", strImg);
        sql.ExecuteNonQuery();
        con.Close();

        File1.PostedFile.SaveAs(Server.MapPath("Images\\")+ strImg);

    }
}