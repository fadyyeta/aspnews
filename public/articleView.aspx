﻿<%@ Page Title="" Language="C#" MasterPageFile="~/public/MasterPage.master" AutoEventWireup="true" CodeFile="articleView.aspx.cs" Inherits="public_articleView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 258px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="details" runat="server">
    </div>
    <hr />
    <br />
    <div id="comments" runat="server">
    </div>
    <hr />    
    <br />
        <table style="width:100%;">
            <tr>
                <td class="auto-style1">name</td>
                <td>
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">comment</td>
                <td>
                    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>
                    <asp:Button ID="btnAddComment" runat="server" Text="Add Comment" OnClick="btnAddComment_Click" />
                </td>
            </tr>
        </table>
    
</asp:Content>

