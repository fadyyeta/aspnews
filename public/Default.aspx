﻿<%@ Page Title="" Language="C#" MasterPageFile="~/public/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="public_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="../slider/engine1/style.css" />
	<script type="text/javascript" src="../slider/engine1/jquery.js"></script>
	<!-- End WOWSlider.com HEAD section -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div id="n" runat="server">

    </div>
    <br />
    <hr />
    <div class="container-slider" style="background-color:#999999;margin:0">
        <!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container1">
	<div class="ws_images"><ul>
		<li><img src="../slider/images/1.jpg" alt="1" title="First image" id="wows1_0"/></li>
		<li><img src="../slider/images/2.jpg" alt="2" title="Second image" id="wows1_1"/></li>
		<li><img src="../slider/images/3.jpg" alt="3" title="Third image" id="wows1_2"/></li>
		<li><img src="../slider/images/4.jpg" alt="slider" title="Fourth image" id="wows1_3"/></li>
		<li><img src="../slider/images/5.jpg" alt="5" title="Fifth image" id="wows1_4"/></li>
	</ul></div>
	<div class="ws_bullets"><div>
		<a href="#" title="1"><span><img src="../slider/data1/tooltips/1.jpg" alt="1"/>First image</span></a>
		<a href="#" title="2"><span><img src="../slider/data1/tooltips/2.jpg" alt="2"/>Second image</span></a>
		<a href="#" title="3"><span><img src="../slider/data1/tooltips/3.jpg" alt="3"/>Third image</span></a>
		<a href="#" title="4"><span><img src="../slider/data1/tooltips/4.jpg" alt="4"/>Fourth image</span></a>
		<a href="#" title="5"><span><img src="../slider/data1/tooltips/5.jpg" alt="5"/>Fifth image</span></a>
	</div></div><div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">slider html</a> by WOWSlider.com v8.8</div>
	<div class="ws_shadow"></div>
	</div>	
	<script type="text/javascript" src="../slider/engine1/wowslider.js"></script>
	<script type="text/javascript" src="../slider/engine1/script.js"></script>
	<!-- End WOWSlider.com BODY section -->

    </div>
</asp:Content>

