﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



public partial class public_articleView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            SqlConnection con = new SqlConnection("server=.;Integrated Security=SSPI; database=News");
            con.Open();

            SqlCommand sql = new SqlCommand("select * from articles where id=" + Request.Params["x"], con);
            SqlDataReader datread = sql.ExecuteReader();

            while (datread.Read())
            {
                details.InnerHtml += "<b>" + datread["title"] + "</b><br>";
                details.InnerHtml += datread["details"] + "<br>";
                details.InnerHtml += "<img src=../WCM/Images/" + datread["photo"] + " height=200px; width=200px; /> <br>";
            }
            con.Close();
            con.Open();

            SqlCommand sql2 = new SqlCommand("select * from comments where id=" + Request.Params["x"], con);
            SqlDataReader datread2 = sql2.ExecuteReader();

            while (datread2.Read())
            {
                comments.InnerHtml += "<b>" + datread2["name"] + " :</b>" +" " + datread2["comment"] + "<br>";
            }
            con.Close();
        }
    }
    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        comments.InnerHtml += "<b>" + txtName.Text + " :</b>" + " " + txtComment.Text + "<br>";
        SqlConnection con = new SqlConnection("server=.;Integrated Security=SSPI; database=News");
        con.Open();

        SqlCommand sql = new SqlCommand("insert into comments (name,comment,id) values (@name,@comment,@id)", con);
        
        sql.Parameters.AddWithValue("@name", txtName.Text);
        sql.Parameters.AddWithValue("@comment", txtComment.Text);
        sql.Parameters.AddWithValue("@id", Request.Params["x"]);
        sql.ExecuteNonQuery();
        con.Close();
        txtComment.Text = txtName.Text = "";
        
    }
}